package dataproviders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
	private Properties properties;
	private final String propertyFilePath = System.getProperty("user.dir") + "/configs/configurations.properties";

	// *************************************************
	private static ConfigFileReader configFileReader;

	public static ConfigFileReader getConfigFileReader() {

		if (configFileReader != null) {
			return configFileReader;
		} else {
			configFileReader = new ConfigFileReader();
			return configFileReader;
		}

	}
	// *************************************************

	public ConfigFileReader() {

		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}
	}


	public String getDriverPath() {
		String driverPath = System.getProperty("user.dir")+"/src/test/resources/drivers/"+properties.getProperty("driverpath");
		if (driverPath != null)
			return driverPath;
		else
			throw new RuntimeException("driverPath not specified in the Configuration.properties file.");
	}

		
	
	public String getApplicationUrl() {

		String url = null;
		try {
			
			url = System.getenv("APPURL");
			
		} catch (Exception e) {
			url = null;
		}
		if (url == null) {
			
			url = properties.getProperty("url");
		}

		if (url != null)
			return url;
		else
			throw new RuntimeException("url not specified in the Configuration.properties file.");
	}

	public int getWebDriverWait() {
		String webdriverwait = properties.getProperty("webdriverwait");
		if (webdriverwait != null)
			return Integer.parseInt(webdriverwait);
		else
			throw new RuntimeException("webdriverwait not specified in the Configuration.properties file.");
	}

	
	public String modeOfExecution()
	{
		String modeOfexe = properties.getProperty("modeOfExecution");
		if (modeOfexe != null)
			return modeOfexe;
		else
			throw new RuntimeException(
					"Browser Name Key value in Configuration.properties is not matched : " + modeOfexe);
	}

	public String getBrowser() {
		String browserName = properties.getProperty("browser");
		if (browserName != null)
			return browserName;
		else
			throw new RuntimeException(
					"Browser Name Key value in Configuration.properties is not matched : " + browserName);
	}
		
	public String getTestDataResourcePath(){
        String testDataResourcePath = properties.getProperty("JsonDataPath");
        if(testDataResourcePath!= null) return testDataResourcePath;
        else throw new RuntimeException("Test Data Resource Path not specified in the Configuration.properties file for the Key:testDataResourcePath");        
    }

}
