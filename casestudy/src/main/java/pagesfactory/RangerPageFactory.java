package pagesfactory;

import org.openqa.selenium.WebDriver;

import pageclass.YahooSignUpPage;


public class RangerPageFactory {



	private YahooSignUpPage yahooSignUpPage;


	private WebDriver driver;
	
	public RangerPageFactory(WebDriver driver) {
		this.driver = driver;
	}


	public YahooSignUpPage getGmailLoginPage() {
		
		return (yahooSignUpPage == null) ? yahooSignUpPage = new YahooSignUpPage(driver) : yahooSignUpPage;
	}

	
}
