package utilities;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


import cucumber.api.Scenario;
import dataproviders.ConfigFileReader;
import drivermanager.WebDriverManager;

/**
 * 
 * @author avinash
 * @category helper class
 */
public class SeleniumHelper {

	WebDriver driver;
	Properties properties;
	public static Scenario scenario;

	//################################################

	
	
	/**
	 * @author Avinash
	 * @param driver
	 * @category constructor
	 */
	public SeleniumHelper(WebDriver driver) {
		this.driver = driver;
		this.driver.manage().window().maximize();
		this.driver.manage().deleteAllCookies();
	}
	
	/**
	 * @name isEnabled
	 * @author Avinash
	 * @description Checks whether webelement is enabled or not
	 * @param element WebElement as parameter
	 * @param assertionMsg description or name of the webElement
	 * @return true if element is enabled else false if it is not enabled
	 * @exception raises exception if steps fails in current method
	 */

	public boolean isEnabled(WebElement element, String assertionMsg ) {


		boolean temp = false;
		try {
			if (element.isEnabled()) {
				temp = true;
				Log.info("Element is enabled");
				return temp;
			}
		} catch (Exception e) {
			Log.error("In isenabled method "+e.getMessage());
			Log.info("Element is not enabled");
			raiseException(assertionMsg);
		}
		return temp;

	}

	/**
	 * 
	 * @name isExists
	 * @author Avinash
	 * @description checks whether WebElement exists or not
	 * @param element WebElement as parameter
	 * @param assertionMsg description or name of the webElement
	 * @return true if element exists else false
	 * @exception raises exception if steps fails in current method
	 */

	protected boolean isExists(WebElement element, String assertionMsg) {


		boolean eleExists=false;

		try {
			if (element.getText() != null) {
				eleExists = true;
				Log.info("Element exists");
			}
		} catch (NoSuchElementException e) {
			Log.error("Element does not Exist " +e.getMessage());
			raiseException(assertionMsg);
		}
		return eleExists;

	}
	/**
	 * 
	 * @name : SelectCheckBox
	 * @author Avinash
	 * @description selects a check box
	 * @param element - WebElement as parameter    
	 * @param strFieldName- name of the object which need to be selected
	 * @exception raises exception if steps fails in current method
	 * @return - void
	 */
	public void SelectCheckBox(WebElement element, String strFieldName) {
		try {
			if (this.isExists(element,strFieldName)) {// System.out.println("Before enter data");
				if (this.isEnabled(element,strFieldName)) {// System.out.println("Before enter data");

					if (!element.isSelected()) {
						try
						{
							element.click();
						}
						catch (Exception e) {
							((JavascriptExecutor) WebDriverManager.getDriver()).executeScript("arguments[0].click();", element);
						}
						Log.info(strFieldName + " checkbox is selected");
						scenario.write(strFieldName + " checkbox is selected");
					}
				} else

					Log.error(strFieldName + " checkbox is not enabled");
				scenario.write(strFieldName + " checkbox is not enabled");
			} else
				Log.error(strFieldName + " checkbox is not found");
			scenario.write(strFieldName + " checkbox is not found");
		} catch (Exception e) {
			raiseException("Exception generated while working with checkbox. " + strFieldName + " is NOT selected and its element is "+ strFieldName);
		}

	}

	/**
	 * @name : selectListValuebyIndex
	 * @author Avinash
	 * @description selects the element from a dropdown based on index
	 * @param element - WebElement
	 * @param strFieldName- Name of the dropdown
	 * @param index- index of the element to select - starts with 0
	 * @exception raises exception if steps fails in current method
	 * @return - void
	 */
	public void selectListValuebyIndex(WebElement element, String strFieldName, int index) {
		try {
			if (this.isExists(element,strFieldName)) {
				new Select(element).selectByIndex(index);
				String selectedOption = new Select(element).getFirstSelectedOption().getText();
				if (selectedOption != null) {
					Log.info(strFieldName + "  is selected");
					scenario.write(strFieldName + "  is selected");
				} else {
					Log.error(strFieldName + "  is not selected");
					scenario.write(strFieldName + "  is not selected");
				}
			}
			else
			{
				Log.info(strFieldName + "  is not found");
			}
		} catch (Exception e) {
			Log.error("Exception generated while working with dropdown. " + strFieldName + " is NOT selected and its element is "+ strFieldName);
			raiseException("Exception generated while working with dropdown. " + strFieldName + " is NOT selected and its element is "+ strFieldName);
		}
	}
	
	/**
	 * 
	 * @name selectListValuebyIndex
	 * @author Avinash
	 * @param element - WebElement as String
	 * @param strFieldName - Value to be selected
	 * @param index - index of the element to select - starts with 0
	 * @exception raises exception if steps fails in current method
	 */

	public void selectListValuebyIndex(String element, String strFieldName, int index) {

		WebElement tempEle = getElement(element);
		try {
			if (this.isExists(tempEle,strFieldName)) {
				new Select(tempEle).selectByIndex(index);
				String selectedOption = new Select(tempEle).getFirstSelectedOption().getText();
				if (selectedOption != null) {
					Log.info(strFieldName + "  is selected");
					scenario.write(strFieldName + "  is selected");
				} else
					Log.error(strFieldName + "  is not selected");
				scenario.write(strFieldName + "  is not selected");
			}
			else
			{
				Log.error(strFieldName + "  is not found");
			}
		} catch (Exception e) {
			Log.error("Exception generated while working with dropdown. " + strFieldName + " is NOT selected and its element is "+ strFieldName);
			raiseException("Exception generated while working with dropdown. " + strFieldName + " is NOT selected and its element is "+ strFieldName);
		}
	}
	
	/**
	 * @name selectListValuebyVisbileText
	 * @description selecting value by visible text
	 * @author Avinash
	 * @param element- WebElement as parameter
	 * @param strFieldName- Value to be selected
	 * @param description - information related to the value which is being selected
	 * @exception raises exception if steps fails in current method
	 * 
	 */

	public void selectListValuebyVisbileText(WebElement element, String strFieldName, String description) {
		String selectedOption = null;
		try {
			if (this.isExists(element,strFieldName)) {
				new Select(element).selectByVisibleText(strFieldName);
				selectedOption = new Select(element).getFirstSelectedOption().getText();
				if (selectedOption != null) {
					Log.info(strFieldName + "  is selected");
					scenario.write(strFieldName + "  is selected");
				} else {
					Log.error(strFieldName + "  is not selected");
					scenario.write(strFieldName + "  is not selected");
				}
			}else
			{
				Log.error(strFieldName + "  is not found");
			}
		} catch (Exception e) {
			Log.error("Exception generated while working with dropdown. " + strFieldName + " is NOT selected and its element is "+ strFieldName);
			raiseException("Exception generated while working with dropdown. " + strFieldName + " is NOT selected and its element is "+ strFieldName);

		}
	}
	
	/**
	 * @name selectListValuebyVisbileText
	 * @description selecting value by visible text
	 * @param element- WebElement as String
	 * @param strFieldName- Value to be selected
	 * @param description - information related to the value which is being selected
	 * @exception raises exception if steps fails in current method
	 * @author Avinash
	 * 
	 */
	

	public void selectListValuebyVisbileText(String element, String strFieldName, String description) {
		WebElement tempEle = getElement(element);
		String selectedOption = null;
		try {
			if (this.isExists(tempEle,strFieldName)) {
				new Select(tempEle).selectByVisibleText(strFieldName);
				selectedOption = new Select(tempEle).getFirstSelectedOption().getText();
				if (selectedOption != null) {
					Log.info(strFieldName + "  is selected");
					scenario.write(strFieldName + "  is selected");
				} else {
					Log.error(strFieldName + "  is not selected");
					scenario.write(strFieldName + "  is not selected");
				}
			}else
			{
				Log.error(strFieldName + "  is not found");
			}
		} catch (Exception e) {
			Log.error("Exception generated while working with dropdown. " + strFieldName + " is NOT selected and its element is "+ strFieldName);
			raiseException("Exception generated while working with dropdown. " + strFieldName + " is NOT selected and its element is "+ strFieldName);

		}
	}

	/**
	 * @name : selectListValuebyValue
	 * @author Avinash
	 * @description gets the value entered in text box
	 * @param element
	 *       - PO
	 * @param strFieldName
	 *            - Name of the element
	 * @return - void
	 */
	public void selectListValuebyValue(WebElement element, String strFieldName) {
		String selectedOption = null;
		try {
			if (this.isExists(element,strFieldName)) {
				new Select(element).selectByValue(strFieldName);
				// waitForPageLoad();
				selectedOption = new Select(element).getFirstSelectedOption().getText();
				if (selectedOption != null) {
					Log.info(strFieldName + "  is selected");
					scenario.write(strFieldName + "  is selected");
				} else {
					Log.error(strFieldName + "  is not selected");
					scenario.write(strFieldName + "  is not selected");
				}
			}else
			{
				Log.error(strFieldName + "  is not found");
			}
		} catch (Exception e) {
			Log.error("Exception generated while working with dropdown. " + strFieldName + " is NOT selected and its element is "+ strFieldName);
			raiseException("Exception generated while working with dropdown. " + strFieldName + " is NOT selected and its element is "+ strFieldName);
		}
	}

	
	/**
	 * @name : selectListValuebyValue
	 * @author Avinash
	 * @description gets the value entered in text box
	 * @param element
	 *       - as string parameter
	 * @param strFieldName
	 *            - Name of the element
	 * @return - void
	 */
	public void selectListValuebyValue(String element, String strFieldName) {
		String selectedOption = null;
		/* WebElement tempEle=getElement(element); */
		try {
			if (this.isExists(WebDriverManager.getDriver().findElement(By.xpath(element)),strFieldName)) {
				new Select(driver.findElement(By.xpath(element))).selectByValue(strFieldName);
				// waitForPageLoad();
				selectedOption = new Select(driver.findElement(By.xpath(element))).getFirstSelectedOption().getText();
				if (selectedOption != null) {
					Log.info(strFieldName + "  is selected");
					scenario.write(strFieldName + "  is selected");
				} else {
					Log.error(strFieldName + "  is not selected");
					scenario.write(strFieldName + "  is not selected");
				}
			}else
			{
				Log.error(strFieldName + "  is not found");
			}
		} catch (Exception e) {

			Log.error("Exception generated while working with dropdown. " + strFieldName + " is NOT selected and its element is "+ strFieldName);
			raiseException("Exception generated while working with dropdown. " + strFieldName + " is NOT selected and its element is "+ strFieldName);
		}
	}




	
	
	/**
	 * @author Avinash
	 * @name getElement
	 * @description converting WebElement as String to WebElement
	 * @param strElement
	 * @return
	 */

	public WebElement getElement(String strElement) {
		WebElement tempWebElement = null;
		WebDriverWait wait = new WebDriverWait(WebDriverManager.getDriver(), ConfigFileReader.getConfigFileReader().getWebDriverWait());
		try {
			String tempElementPrefix, tempElementId;
			String[] tempElement;

			tempElement = strElement.split(";");
			tempElementPrefix = tempElement[0];
			tempElementId = tempElement[1];

			if (tempElementPrefix.toLowerCase().trim().startsWith("xpath")) {
				tempWebElement = wait.until(ExpectedConditions.presenceOfElementLocated((By.xpath(tempElementId))));
			} else if (tempElementPrefix.toLowerCase().trim().startsWith("id")) {
				tempWebElement = wait.until(ExpectedConditions.presenceOfElementLocated((By.id(tempElementId))));
			} else if (tempElementPrefix.toLowerCase().trim().startsWith("link")) {
				tempWebElement = wait.until(ExpectedConditions.presenceOfElementLocated((By.linkText(tempElementId))));
			} else if (tempElementPrefix.toLowerCase().trim().startsWith("css")) {
				tempWebElement = wait
						.until(ExpectedConditions.presenceOfElementLocated((By.cssSelector(tempElementId))));
			} else if (tempElementPrefix.toLowerCase().trim().startsWith("name")) {
				tempWebElement = wait.until(ExpectedConditions.presenceOfElementLocated((By.name(tempElementId))));
			} else if (tempElementPrefix.toLowerCase().trim().startsWith("tag")) {
				tempWebElement = wait.until(ExpectedConditions.presenceOfElementLocated((By.tagName(tempElementId))));
			} else if (tempElementPrefix.toLowerCase().trim().startsWith("class")) {
				tempWebElement = wait.until(ExpectedConditions.presenceOfElementLocated((By.className(tempElementId))));
			}
		} catch (Exception e) {

			raiseException("Exception generated while retrieving the webelement from string format for :"+ strElement);
		}
		return tempWebElement;

	}
	
	/**
	 * @author Avinash
	 * @name enterText
	 * @description Enters Text to the input field
	 * @param element- WebElement as String
	 * @param strText- Text to be entered in input field
	 * @param description- passing webelement name as description
	 */

	public void enterText(String element, String strText, String description) {


		WebElement tempWebElement = getElement(element);
		try {

			WebDriverWait wait = new WebDriverWait(WebDriverManager.getDriver(), ConfigFileReader.getConfigFileReader().getWebDriverWait());

			if (isExists(tempWebElement,description)) {
				if (this.isEnabled(tempWebElement,description)) {
					wait.until(ExpectedConditions.elementToBeClickable((driver.findElement(By.xpath(element)))));
					try
					{
						tempWebElement.click();
					}
					catch (Exception e) {

						((JavascriptExecutor) WebDriverManager.getDriver()).executeScript("arguments[0].click();", tempWebElement);
					}
					tempWebElement.clear();
					tempWebElement.clear();
					WebDriverManager.getDriver().manage().timeouts().implicitlyWait(8, TimeUnit.SECONDS);
					try
					{
						tempWebElement.click();
					}
					catch (Exception e) {

						((JavascriptExecutor) WebDriverManager.getDriver()).executeScript("arguments[0].click();", tempWebElement);
					}
					tempWebElement.clear();
					tempWebElement.clear();

					try {
						tempWebElement.sendKeys(strText);
					}catch(Exception e)
					{

						JavascriptExecutor js = (JavascriptExecutor) WebDriverManager.getDriver();
						js.executeScript("arguments[0].value='" + strText + "';", tempWebElement);
					}
					Log.info("Entered the "+description + " as: " + strText);
					scenario.write("Entered the "+description + " as: " + strText);

				} else

					Log.error(description + " is not enabled");
			} else

				Log.error(description + " is not found");
			WebDriverManager.getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} catch (Exception e) {
			Log.error("Exception generated while entering the text : " + strText + " and its element is: " + description);
			raiseException("Exception generated while entering the text : " + strText + " and its element is: " + description);
		}
	}

	

	/**
	 * @author Avinash
	 * @name enterText
	 * @description Enters Text to the input field
	 * @param element- WebElement as parameter
	 * @param strText- Text to be entered in input field
	 * @param description- passing webelement name as description
	 */


	public void enterText(WebElement element, String strText, String description) {
		try {

			WebDriverWait wait = new WebDriverWait(WebDriverManager.getDriver(), ConfigFileReader.getConfigFileReader().getWebDriverWait());

			if (isExists(element,description)) {
				if (isEnabled(element,description)) {
					wait.until(ExpectedConditions.elementToBeClickable(element));
					try
					{
						element.click();

					}
					catch (Exception e) {

						((JavascriptExecutor) WebDriverManager.getDriver()).executeScript("arguments[0].click();", element);Log.info(description + " text  field is clicked");

					}

					element.clear();

					element.clear();

					try
					{

						element.click();
						Log.info(description + " text  field is clicked");
					}
					catch (Exception e) {

						((JavascriptExecutor) WebDriverManager.getDriver()).executeScript("arguments[0].click();", element);Log.info(description + " text  field is clicked");

					}

					element.clear();

					element.clear();


					try {
						element.sendKeys(strText);

					}catch(Exception e)
					{

						JavascriptExecutor js = (JavascriptExecutor) WebDriverManager.getDriver();
						js.executeScript("arguments[0].value='" + strText + "';", element);

					}

					Log.info("Entered the "+description + " as: " + strText);
					scenario.write("Entered the "+description + " as: " + strText);
				}

			}
		} catch (Exception e) {
			Log.error("Exception generated while entering the text : " + strText + " and its element is: " + description);
			raiseException("Exception generated while entering the text : " + strText + " and its element is: " + description);
		}
	}

	
	
	/**
	 * @name click
	 * @description clicking on webElement
	 * @param element- WebElement as string parameter
	 * @param description- Name of the Webelement
	 * @author Avinash
	 */
	public void click(String element, String description) {
		WebElement tempWebElement = null;

		try {
			tempWebElement = getElement(element);
			WebDriverWait wait = new WebDriverWait(WebDriverManager.getDriver(), ConfigFileReader.getConfigFileReader().getWebDriverWait());

			if (isExists(tempWebElement,description)) {
				if (isEnabled(tempWebElement,description)) {
					wait.until(ExpectedConditions.elementToBeClickable(tempWebElement));
					try
					{

						tempWebElement.click();

					}
					catch (Exception e) {

						((JavascriptExecutor) WebDriverManager.getDriver()).executeScript("arguments[0].click();", tempWebElement);

					}
					Log.info(description + " is clicked");
					scenario.write(description + " is clicked");
				}else
				{
					Log.error(description + " is NOT enabled");
					scenario.write(description + " is NOT enabled");
				}

			}else {

				Log.error(description + " is NOT found");
				scenario.write(description + " is NOT found");
			}

			try {
				WAITPAGELOAD();
			} catch (Exception e1) {
				e1.printStackTrace();
			}

		} catch (Exception e) {

			Log.error("Exception generated while working with Click. Element is" + description);
			raiseException("Exception generated while working with Click. Element is" + description);
		}

	}
	
	
	/**
	 * @name click
	 * @description clicking on webElement
	 * @param element- WebElement as parameter
	 * @param description- Name of the Webelement
	 * @author Avinash
	 */

	public void click(WebElement element, String description) {
		try {

			WebDriverWait wait = new WebDriverWait(WebDriverManager.getDriver(), ConfigFileReader.getConfigFileReader().getWebDriverWait());

			if (isExists(element,description)) {
				if (isEnabled(element,description)) {
					wait.until(ExpectedConditions.elementToBeClickable(element));
					try
					{

						element.click();

					}
					catch (Exception e) {

						((JavascriptExecutor) WebDriverManager.getDriver()).executeScript("arguments[0].click();", element);
					}
					Log.info(description + " is clicked");
					scenario.write(description + " is clicked");
				} else {
					Log.error(description + " is NOT enabled");
					scenario.write(description + " is NOT enabled");

				}
			}
			else
			{
				Log.error(description + " is NOT found");
				scenario.write(description + " is NOT found");
			}
			WAITPAGELOAD();

		} catch (Exception e) {
			Log.error("Exception generated while working with Click. Element is" + description);
			raiseException("Exception generated while working with Click. Element is" + description);
		}

	}

	/**
	 * @name getText
	 * @param element- Webelement as String
	 * @param description- Name of the webelement
	 * @return
	 */

	public String getText(String element, String description) {
		WebElement temEle;
		String text = "";
		WebDriverWait wait = new WebDriverWait(WebDriverManager.getDriver(), ConfigFileReader.getConfigFileReader().getWebDriverWait());
		try {
			temEle = getElement(element);
			if (isExists(temEle,description)) {
				if(isEnabled(temEle,description))
				{
					wait.until(ExpectedConditions.visibilityOf(temEle));
					text = temEle.getText();
				} else {
					Log.error(description + " is not enabled");
				}
			}else
			{
				Log.error(description + " is not found");
			}
		} catch (Exception e) {

			Log.error("Exception generated while retrieving the text from string type webelement. Element is" + description);
			raiseException("Exception generated while retrieving the text from string type webelement. Element is" + description);
		}

		return text;
	}
	
	/**
	 * @name getText
	 * @param element- Webelement as String
	 * @param description- Name of the webelement
	 * @return
	 */

	public String getText(WebElement element, String description) {

		String text = "";
		WebDriverWait wait = new WebDriverWait(WebDriverManager.getDriver(), ConfigFileReader.getConfigFileReader().getWebDriverWait());
		try {
			if (isExists(element,description)) {
				if(isEnabled(element,description))
				{
					wait.until(ExpectedConditions.visibilityOf(element));
					text = element.getText();
				} else {
					Log.error(description + " is not enabled");
				}
			}else
			{
				Log.error(description + " is not found");
			}
		} catch (Exception e) {
			Log.error("Exception generated while retrieving the text from webelement. Element is" + description);
			raiseException("Exception generated while retrieving the text from webelement. Element is" + description);
		}
		return text;
	}


	/**
	 * @name isElementVisible
	 * @description checks whether webelement is visible or not
	 * @param element - WebElement as String 
	 * @param description- Name of the Webelement
	 * @author Avinash
	 */

	public boolean isElementVisible(String element, String description) {
		WebElement tempEle = getElement(element);
		Boolean eleStat = false;
		try {

			WebDriverWait wait = new WebDriverWait(WebDriverManager.getDriver(), ConfigFileReader.getConfigFileReader().getWebDriverWait());
			if (isExists(tempEle,description)) {
				if(isEnabled(tempEle,description))
				{
					wait.until(ExpectedConditions.visibilityOf(WebDriverManager.getDriver().findElement(By.xpath(element))));
					if (tempEle.isDisplayed()) {
						scenario.write(description + " is visible");
						Log.info(description + " is displayed");
						eleStat = true;
					} else {
						scenario.write(description + " is not enabled");
						Log.info(description + " is not enabled");
						eleStat = false;
					}
				}else
				{
					scenario.write(description + " is not found");
					Log.error(description + " is not found");
				}
			}

		} catch (Exception e) {
			Log.error("Exception generated while checking the element visiblity. Element " +description + " is not displayed");
			raiseException("Exception generated while checking the element visiblity. Element " +description + " is not displayed");

		}
		return eleStat;

	}

	
	/**
	 * @name isElementVisible
	 * @description checks whether webelement is visible or not
	 * @param element - WebElement as parameter
	 * @param description- Name of the Webelement
	 * @author Avinash
	 */

	public void isElementVisible(WebElement element, String description) {

		try {
			WebDriverWait wait = new WebDriverWait(WebDriverManager.getDriver(), ConfigFileReader.getConfigFileReader().getWebDriverWait());
			wait.until(ExpectedConditions.visibilityOf(element));

			if (isExists(element,description)) {
				if(isEnabled(element, description))
				{
					if (element.isDisplayed()) 
					{
						scenario.write(description + " is visible");
						Log.info(description + " is visible");
					} else {

						Log.error(description + " is not enabled");
						scenario.write(description + " is enabled");
					}
				}
				else
				{
					Log.error(description + " is not found");
					scenario.write(description + " is found");
				}
			}
		} catch (Exception e) {
			Log.error("Exception generated while checking the element visiblity. Element " +description + " is not displayed");
			raiseException("Exception generated while checking the element visiblity. Element " +description + " is not displayed");

		}

	}

	// ################################################################################



/**
 * @name takeScreenShot
 */

	public void takeScreenShot() {

		try {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "/target/screenshot/pageScreenshot.png"), true);
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png"); // stick it in the report


		} catch (Exception e) {
			e.printStackTrace();
		}

	}



	
	/**
	 * 
	 */
	protected void cleanup() {
		WebDriverManager.closeDriver();

	}

	
	/**
	 * 
	 * @return
	 */
	protected WebDriver getDriver() {
		return WebDriverManager.getDriver();
	}
	
	/**
	 * 
	 * @param scenario
	 */

	public static void setScenario(Scenario scenario) {
		SeleniumHelper.scenario = scenario;
	}



	
	/**
	 * @name getJsonObject
	 * @description get json object from the json file
	 * @param Filename - path to the json file
	 * @return Json Object
	 * @author Avinash
	 */
	
	public  static Object getJsonObject(String Filename) 
	{
		Object obj = null;
		try {
			obj = new JSONParser().parse(new FileReader(ConfigFileReader.getConfigFileReader().getTestDataResourcePath()+Filename));

		} catch (IOException | org.json.simple.parser.ParseException e) {

			e.printStackTrace();
		}

		return obj;
	}

	/**
	 * 
	 * @name getJsonValues
	 * @description get json value from json Object
	 * @param obj Json Object as parameter
	 * @param key of type String
	 * @return ArrayList<String>
	 * @throws IOException
	 * @author Avinash
	 */

	public  ArrayList<String> getJsonValues(Object obj,String key) throws IOException
	{
		ArrayList<String> mainarr=new ArrayList<String>();

		if (obj instanceof JSONObject)

		{
			mainarr=printJsonObject((JSONObject)obj,key);
		}else if(obj instanceof JSONArray)
		{
			mainarr=printJsonArray((JSONArray)obj,key);
		}

		return mainarr;



	}
	
	/**
	 * @name printJsonObject
	 * @description prints the JSON Object values by parsing the JSON file
	 * @param jsArr - Passing Json Object as parameter
	 * @param expKey - Passing String as Expected Key from json file
	 * @return ArrayList<String>
	 * @throws IOException
	 * @author Avinash
	 */

	public  ArrayList<String> printJsonObject(JSONObject jsObj,String expKey) throws IOException
	{

		ArrayList<String> jsobarr=new ArrayList<String>();

		for (Object key : jsObj.keySet()) {

			//based on you key types
			String  keyStr = (String)key;
			Object keyvalue = jsObj.get(keyStr);

			//Print key and value
			//            System.out.println(keyStr + " : " + keyvalue);

			if(expKey.equals(keyStr))
			{
				jsobarr.add(keyvalue.toString());
				break;
			}


			if (keyvalue instanceof JSONObject)
			{

				jsobarr=printJsonObject((JSONObject)keyvalue,expKey);

			}else if(keyvalue instanceof JSONArray)
			{

				//                System.out.print(keyStr +"");
				jsobarr=printJsonArray((JSONArray)keyvalue,expKey);
				if(jsobarr.isEmpty())
				{

				}else
				{
					return jsobarr;
				}
			}


		}

		return jsobarr;
	}

	
	
	/**
	 * @name printJsonArray
	 * @description prints the JSON Array values by parsing the JSON file
	 * @param jsArr - Passing Json Array as parameter
	 * @param expKey - Passing String as Expected Key from json file
	 * @return ArrayList<String>
	 * @throws IOException
	 * @author Avinash
	 */

	public ArrayList<String> printJsonArray(JSONArray jsArr,String expKey) throws IOException
	{

		ArrayList<String> jsoArarr=new ArrayList<String>();

		for (int i = 0; i < jsArr.size(); i++) {



			if(jsArr.get(i)  instanceof JSONObject)
			{
				JSONObject objects = (JSONObject) jsArr.get(i);
				for (Object key : objects.keySet()) {


					//based on you key types
					String  keyStr = (String)key;
					Object keyvalue = objects.get(keyStr);

					//Print key and value
					//                System.out.println(keyStr + " : " + keyvalue);

					if(expKey.equals(keyStr))
					{
						jsoArarr.add(keyvalue.toString());
					}
				}

			}

		}


		return jsoArarr;
	}


	



	
	/**
	 * @name raiseException
	 * @description Takes screenshot
	 * and Closes webDriver instances
	 * and writes the assertion message to the report
	 * @param assertionMsg description of the issue 
	 * @author Avinash
	 * 
	 */
	public void raiseException(String assertionMsg) {


			takeScreenShot();
			cleanup();
			scenario.write(assertionMsg);
			Assert.assertTrue(assertionMsg, false);


	}
	
	

	/**
	 * @name isElementPresent
	 * @description checking whether webelement is present or not
	 * @param Webelement as parameter
	 * @return true if WebElement is present else false
	 * @author Avinash
	 */
	
	
	protected boolean isElementPresent(WebElement elem)
	{
		boolean x= false;
		try
		{

			if(elem.isDisplayed())
			{
				x=true;
			}

		}catch(NoSuchElementException nse)
		{
			x=false;
		}

		return x;

	}

/**
 * @name WAITPAGELOAD
 * @description waits till the page loads completely
 * @return true if page is loaded completely in given time else false
 * @throws Exception
 * @author Avinash
 */
	public static boolean WAITPAGELOAD() throws Exception{
		
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver driver)
			{
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}

		};

		WebDriverWait wait=new WebDriverWait(WebDriverManager.getDriver(),120);
		boolean bRet=(wait.until(pageLoadCondition));
		Log.info("Checking if progress bar is there.....");
		try
		{
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='bar']")));

		}catch(Exception eo)
		{
			Log.error("Exception caught in waitForPageLoad");
		}
		if(bRet)
		{
			Log.info("Ready to execute now..");
			return bRet;
		}
		else
			return false;
	}



}