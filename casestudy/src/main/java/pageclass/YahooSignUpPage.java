package pageclass;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import dataproviders.ConfigFileReader;
import drivermanager.WebDriverManager;
import pagesfactory.RangerPageFactory;
import utilities.SeleniumHelper;

public class YahooSignUpPage extends SeleniumHelper {

	WebDriver driver = WebDriverManager.getDriver();
	RangerPageFactory pagefactory = new RangerPageFactory(driver);
    WebDriverWait wait=new WebDriverWait(driver, ConfigFileReader.getConfigFileReader().getWebDriverWait());

	public YahooSignUpPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	
	@FindBy(how = How.XPATH, using = "//input[@id='usernamereg-firstName']")
	private WebElement firstNameTxtField;
	
	@FindBy(how = How.XPATH, using = "//a[@id='usernamereg-altreg']")
	private WebElement yahooemaillink;
	

	@FindBy(how = How.XPATH, using = "//input[@id='usernamereg-lastName']")
	private WebElement lastNameTxtField;

	@FindBy(how = How.XPATH, using = "//input[@id='usernamereg-yid']")
	private WebElement emailAddressTxtField;
	
	@FindBy(how = How.XPATH, using = "//input[@id='usernamereg-phone']")
	private WebElement phoneNumberTxtField;
	
	@FindBy(how = How.XPATH, using = "//input[@id='usernamereg-password']")
	private WebElement passwordTxtField;
	
	@FindBy(how = How.XPATH, using = "//select[@id='usernamereg-month']")
	private WebElement birthMonthField;
	
	@FindBy(how = How.XPATH, using = "//input[@id='usernamereg-day']")
	private WebElement dayTxtField;
	
	@FindBy(how = How.XPATH, using = "//input[@id='usernamereg-year']")
	private WebElement yearTxtField;
	
	@FindBy(how = How.XPATH, using = "//input[@id='usernamereg-freeformGender']")
	private WebElement genderTxtField;
	
	@FindBy(how = How.XPATH, using = "//button[@id='reg-submit-button']")
	private WebElement continueBtn;
	
	@FindBy(how = How.XPATH, using = "//div[@data-error='messages.ERROR_MISSING_FIELD']")
	private WebElement errorMessageField;
	
	@FindBy(how = How.XPATH, using = "//div[@id='reg-error-firstName']")
	private WebElement firstNameErrorMessageField;
	
	@FindBy(how = How.XPATH, using = "//div[@id='reg-error-lastName']")
	private WebElement lastNameErrorMessageField;
	
	@FindBy(how = How.XPATH, using = "//div[@id='reg-error-email']")
	private WebElement emailErrorMessageField;
	
	@FindBy(how = How.XPATH, using = "//div[@id='reg-error-password']")
	private WebElement passwordErrorMessageField;
	
	@FindBy(how = How.XPATH, using = "//div[@id='reg-error-birthDate']")
	private WebElement birthDateErrorMessageField;
	
	
	
	
	
	

	
	
	public void enterFirstName(String firstName)
	{
		enterText(firstNameTxtField, firstName+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()), "entering first name");
	}
	
	public void enterSurname(String lastName)
	{
		enterText(lastNameTxtField, lastName+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()), "entering last name");
	}
	
	
	public void enterEmailAddress(String emailAddress)
	{
		enterText(emailAddressTxtField, emailAddress+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()), "entering email address");
	}
	
	public void enterPassword(String password)
	{
		enterText(passwordTxtField, password, password);
	}
	
	public void selectMonth(String month)
	{
		selectListValuebyVisbileText(birthMonthField, month,month);
	}
	
	public void enterDay(String day)
	{
		enterText(dayTxtField, day, day);
	}
	
	public void enterYear(String year)
	{
		enterText(yearTxtField, year, year);
	}
	
	public void enterGender(String gender)
	{
		enterText(genderTxtField, gender, gender);
	}
	
	public void enterPhoneNumber(String phoneNumber)
	{
		enterText(phoneNumberTxtField, phoneNumber, phoneNumber);
	}
	
	public void clickOnCreateNewYahoolink()
	{
		click(yahooemaillink, "create yahoo link");
	}
	
	public void clickOnContinueButton()
	{
		click(continueBtn, "continue button");
	}
	
	public void verifyErrorMessage()
	{
		isElementVisible(errorMessageField, "error message field");
		isElementVisible(firstNameErrorMessageField, "error message field");
		isElementVisible(lastNameErrorMessageField, "error message field");
		isElementVisible(emailErrorMessageField, "error message field");
		isElementVisible(passwordErrorMessageField, "error message field");
		isElementVisible(birthDateErrorMessageField, "error message field");
		
	}
	

	
	

		
}
