package drivermanager;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import dataproviders.ConfigFileReader;
import utilities.Log;

public class WebDriverManager {

	private static WebDriver driver = null;
	private static String driverType;

	private static Map<String, WebDriver> drivers = new HashMap<String, WebDriver>();

	private static final String CHROME_DRIVER_PROPERTY = "webdriver.chrome.driver";


	public static WebDriver getDriver() {

		driverType = ConfigFileReader.getConfigFileReader().getBrowser();

		switch (driverType) {

		case "chrome":
			driver = drivers.get("Chrome");

			if (driver == null || driver.toString().contains("(null)")) {

				/*********** UI MODE *************/
				if(ConfigFileReader.getConfigFileReader().modeOfExecution().equalsIgnoreCase("UI"))
				{
					
					System.setProperty(CHROME_DRIVER_PROPERTY,
							ConfigFileReader.getConfigFileReader().getDriverPath());
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--incognito");
					DesiredCapabilities capabilities =DesiredCapabilities.chrome();
					capabilities.setCapability(ChromeOptions.CAPABILITY,
							options); driver= new ChromeDriver(capabilities);
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
							driver.manage().window().maximize();
							drivers.put("Chrome", driver);

				}
				
				/*********** HEADLESS BROWSER MODE *************/
				else if(ConfigFileReader.getConfigFileReader().modeOfExecution().equalsIgnoreCase("Headless"))
				{
					System.setProperty(CHROME_DRIVER_PROPERTY, ConfigFileReader.getConfigFileReader().getDriverPath());
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--incognito");
					options.addArguments("--headless");
					options.setBinary("/opt/google/chrome/chrome");
					options.addArguments("--no-sandbox");
					options.addArguments("--disable-setuid-sandbox");
					options.addArguments("--privileged");
					options.addArguments("window-size=1980,960");
					HashMap<String, Object> prefs = new HashMap<String, Object>();
					prefs.put("download.default_directory", System.getProperty("user.dir") + File.separator
							+ "externalFiles" + File.separator + "downloadFiles");
					options.setExperimentalOption("prefs", prefs);
					DesiredCapabilities capabilities = DesiredCapabilities.chrome();
					capabilities.setCapability(ChromeOptions.CAPABILITY, options);
					driver = new ChromeDriver(capabilities);
					driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					drivers.put("Chrome", driver);
					driver.manage().window().maximize();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
				{
					Log.info("Mode of execution is not defined properly");
				}
			}

			break;
		}

		return driver;
	}

	public static void closeDriver() {
		if(driver==null)
		{
		
		}else
		{
		try {
			for(String handle : driver.getWindowHandles())
			{
				driver.switchTo().window(handle);
				driver.close();
			}
			driver.quit();
			driver=null;
		} catch (Exception e) {
			Log.error("Either Driver is not initialized or closed before");
		}

	}
	}

}
