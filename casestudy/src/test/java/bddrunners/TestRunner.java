package bddrunners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {
					"src/test/resources/features/GitHubAPITestingEndPoint.feature"
					},
				plugin= {"json:target/cucumber.json","html:target/site/cucumber-pretty"},
				glue= {"bddstepdefinitions","bddhooks"},
				dryRun= false, 
				monochrome= true
		)

public class TestRunner {
	
}
