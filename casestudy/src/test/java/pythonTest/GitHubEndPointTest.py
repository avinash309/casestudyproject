import requests
import json
import pytest
import allure

APP_URL = "https://api.github.com"

API_EVENTS=APP_URL+"/repos/octokit/octokit.rb/events"

@allure.step('Response')
def getResponse(response):
    pass

@allure.step('Post Request call for')
def POSTRequestValidation(URI,payload):
    response = requests.post(URI, json=payload)
    print(json.dumps(response.json(), indent=4))
    if (response.json()["status"] == "success"):
        pass
    else:
        pytest.fail(response.json()["status"] == "success", pytrace=True)
    return response


@allure.step('GET Request call for')
def GETRequestValidation_step(URI):
    response = requests.get(URI)
    print(json.dumps(response.json(), indent=4))
    # if ((response.json()["status"] == "success") or (response.json()["status"]["success"] == True)):
    #     pass
    # else:
    #     pytest.fail(response.json()["status"] == "success",pytrace=True)
    return response

@allure.step('Delete Request call for')
def DELETERequestValidation(URI,payload):
    response = requests.delete(URI,json=payload)
    print(json.dumps(response.json(), indent=4))
    if (response.json()["status"] == "success"):
        pass
    else:
        pytest.fail(response.json()["status"] == "success", pytrace=True)
    return response

def DELETERequestValidationWithoutPayload(URI):
    response = requests.delete(URI)
    print(json.dumps(response.json(), indent=4))
    if (response.json()["status"] == "success"):
        pass
    else:
        pytest.fail(response.json()["status"] == "success", pytrace=True)
    return response


@allure.step('Events endpoint')
def getEventResponse():

    eventsResponse = GETRequestValidation_step(API_EVENTS)

    return eventsResponse

@allure.step('get all Event types')
def getAllEventsType(eventsResponse):

    data = eventsResponse.json()

    for i in data:
        print(i["type"])

@allure.step('get all Event Logins')
def getAllLogins(eventsResponse):

    data = eventsResponse.json()

    for i in data:
        print(i["actor"]["login"])


##################Calling methods####################################

eventRes = getEventResponse()
getAllEventsType(eventRes)
getAllLogins(eventRes)