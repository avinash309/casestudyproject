package bddstepdefinitions;

import cucumber.api.java.en.Given;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GitHubAPITest {



	@Given("^Hit the endpoint of get events and validate the response$")
	public void hit_the_endpoint_of_get_events_and_validate_the_response() throws Throwable {
		
		
		try {

			String repos = "/repos/octokit/octokit.rb/events";

			RestAssured.baseURI = "https://api.github.com";
			
			RequestSpecification requestImport = RestAssured.given();

			requestImport.header("Content-Type", "application/json");

			Response responseJob_ImportTemplates = requestImport.get(repos);

			System.out.println(responseJob_ImportTemplates.getBody().asString());
			
			

			
		} catch (Exception e) {
			System.out.println("Issue while hitting the given api "+ e);
			
		}




	}
	
}
