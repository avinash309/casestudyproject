package bddstepdefinitions;

import java.io.IOException;
import java.util.ArrayList;

import cucumber.api.java.en.Given;
import dataproviders.ConfigFileReader;
import drivermanager.WebDriverManager;
import pageclass.YahooSignUpPage;
import utilities.SeleniumHelper;

public class YahooSignupStepDef {

	SeleniumHelper rsh=new SeleniumHelper(WebDriverManager.getDriver());
	YahooSignUpPage yahooSignup= new YahooSignUpPage(WebDriverManager.getDriver());
	
	Object obj=SeleniumHelper.getJsonObject("yahooSignup.json");
	static ArrayList<String> firstName=new ArrayList<String>();
	static ArrayList<String> lastName=new ArrayList<String>();
	static ArrayList<String> emailAddress=new ArrayList<String>();
	static ArrayList<String> password=new ArrayList<String>();
	static ArrayList<String> month=new ArrayList<String>();
	static ArrayList<String> day=new ArrayList<String>();
	static ArrayList<String> year=new ArrayList<String>();
	static ArrayList<String> gender=new ArrayList<String>();
	static ArrayList<String> phoneNumber=new ArrayList<String>();
	
	public void jsonUserInfo() {

		try {              
			firstName=rsh.getJsonValues(obj,"firstName");
			lastName=rsh.getJsonValues(obj,"lastName");     
			emailAddress=rsh.getJsonValues(obj,"emailAddress"); 
			password=rsh.getJsonValues(obj,"password"); 
			month=rsh.getJsonValues(obj,"month"); 
			day=rsh.getJsonValues(obj,"day"); 
			year=rsh.getJsonValues(obj,"year"); 
			gender=rsh.getJsonValues(obj,"gender"); 
			phoneNumber=rsh.getJsonValues(obj,"phonenumber");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@Given("^Launch yahoo signup Application Url$")
	public void launch_yahoo_signup_Application_Url() throws Throwable {
		jsonUserInfo();
		WebDriverManager.getDriver().get(ConfigFileReader.getConfigFileReader().getApplicationUrl());
		
	}

	@Given("^provide required fields to signup$")
	public void provide_required_fields_to_signup() throws Throwable {
		yahooSignup.clickOnCreateNewYahoolink();
		yahooSignup.enterFirstName(firstName.get(0).toString());
		yahooSignup.enterSurname(lastName.get(0).toString());
		yahooSignup.enterEmailAddress(emailAddress.get(0).toString());
		yahooSignup.enterPhoneNumber(phoneNumber.get(0).toString());
		yahooSignup.enterPassword(password.get(0).toString());
		yahooSignup.selectMonth(month.get(0).toString());
		yahooSignup.enterDay(day.get(0).toString());
		yahooSignup.enterYear(year.get(0).toString());
		yahooSignup.enterGender(gender.get(0).toString());
		yahooSignup.clickOnContinueButton();
	}
	
	@Given("^check for the error message when value is not provided to the required field for signup$")
	public void check_for_the_error_message_when_value_is_not_provided_to_the_required_field_for_signup() throws Throwable {
	    
		yahooSignup.clickOnContinueButton();
		yahooSignup.verifyErrorMessage();
		
		
	}


	
}
