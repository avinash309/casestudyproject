package bddhooks;

import utilities.*;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import drivermanager.WebDriverManager;

public class Hooks {



	@Before
	public void Before(Scenario scenario) {

		Log.info("In Before hooks");
		String strScenario = scenario.getName();
		SeleniumHelper.setScenario(scenario);
		Log.initializeLog();
		Log.info("**********************************************");
		Log.info(" Started Working on Scenario : " + strScenario);
		Log.info("**********************************************");
		

	}



	@After
	public void After(Scenario scenario) {
		@SuppressWarnings("unused")
		String strScenario = scenario.getName();

		Log.info("In After hooks");
		Log.info("**********************************************");
		Log.info(" Completed Working on Scenario : " + scenario.getName());
		Log.info("**********************************************");
		WebDriverManager.closeDriver();


	}




}
