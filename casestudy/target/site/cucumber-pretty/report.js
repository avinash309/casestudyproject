$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/GitHubAPITestingEndPoint.feature");
formatter.feature({
  "line": 1,
  "name": "Verifying one of the endpoint in GitHub Developer",
  "description": "",
  "id": "verifying-one-of-the-endpoint-in-github-developer",
  "keyword": "Feature"
});
formatter.before({
  "duration": 112812782,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "working on events api from GitHub Developer",
  "description": "",
  "id": "verifying-one-of-the-endpoint-in-github-developer;working-on-events-api-from-github-developer",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "Hit the endpoint of get events and validate the response",
  "keyword": "Given "
});
formatter.match({
  "location": "GitHubAPITest.hit_the_endpoint_of_get_events_and_validate_the_response()"
});
formatter.result({
  "duration": 18651516330,
  "status": "passed"
});
formatter.after({
  "duration": 2874893,
  "status": "passed"
});
});